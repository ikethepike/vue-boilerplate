/**
 * scrolling
 * @return state.scrolling
*/
export const scrolling = (state) => {
    return state.scrolling
}


//////////////////////
// Scrolling getter //
//////////////////////

var prevX       = 0,
    prevY       = 0,
    vertical    = true,
    direction   = null,
    first       = true
/**
 * Returns an object with scroll properties
 * @return state.scroll
*/
export const scroll = (state) => {

    if(first) {
        first = false
        parseScroll(state)
    }

    return state.scroll

}

function parseScroll(state)
{

    var debounced = debounce(function(callback) {

        var scrollX     = window.scrollX,
            scrollY     = window.scrollY

        // Measure which is the bigger difference
        vertical = Math.abs(scrollY + prevY) >= Math.abs(scrollX + prevX) ? true : false

        // Set this as cardinal direction
        if(vertical) {
            direction = scrollY >= prevY ? 'down' : "up"
        } else {
            direction = scrollX >= prevX ? "right" : "left"
        }

        // Set prevs here
        prevX = scrollX
        prevY = scrollY

        state.scroll =  {
            x           : scrollX,
            y           : scrollY,
            direction   : direction,
            momentum    : vertical ? scrollY - prevY : scrollX - prevX
        }

    }.bind(state), 20 /* 30 FPS*/ )

    window.addEventListener("scroll", debounced )

}



/**
 * debounce function
 * @param  func
 * @param  wait
 * @param  immediate
 * @return
 */
function debounce(func, wait, immediate)
{
    var timeout

    return function() {
        var context = this, args = arguments
        var later = function() {
            timeout = null
            if (!immediate) func.apply(context, args)
        }
        var callNow = immediate && !timeout
        clearTimeout(timeout)
        timeout = setTimeout(later, wait)
        if (callNow) func.apply(context, args)
    }
}
