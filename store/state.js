export default {
    scrolling   : true,
    scroll      : {
        x           : 0,
        y           : 0,
        direction   : null,
        momentum    : null,

    }
}