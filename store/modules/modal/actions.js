/**
 * view
 * @param  {[type]} view
 * @return commit
 */
export const view = ({commit}, view) => {
    return commit("view", view)
}

/**
 * close
 * @param  {[type]}
 * @return commit
 */
export const close = ({commit}) => {
    return commit("view", null)
}

/**
 * content
 * @param  {[type]} content
 * @return commit
 */
export const content = ({commit}, content = null) => {
    return commit("content", content)
}