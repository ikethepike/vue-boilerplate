/**
 * content
 * @return state.content
*/
export const content = (state) => {
    return state.content
}

/**
 * view
 * @return state.view
*/
export const view = (state) => {
    return state.view
}

/**
 * Returns content count
 * @return state.count
*/
export const count = (state) => {
    if(!state.content)
        return 0

    return state.content.length
}