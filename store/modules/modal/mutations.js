/**
 * view mutation
 * @param view
 */
export const view = (state, view) => {
    return state.view = view
}

/**
 * content mutation
 * @param content
 */
export const content = (state, content) => {
    return state.content = content
}