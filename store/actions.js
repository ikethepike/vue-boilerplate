/**
 * scrolling
 * @param  {[type]} scrolling
 * @return commit
 */
export const scrolling = ({commit, state}, scrolling = null) => {
    if(typeof scrolling === "null")
        return commit("scrolling", !state.scrolling)

    return commit("scrolling", scrolling)
}

/**
 * scrollTo
 * @param  {[type]} scroll
 * @return commit
 */
export const scrollTo = ({commit}, offset) => {

    var i = 10

    var int = setInterval(function() {

      window.scrollTo(0, i)
      i += 10
      if (i => offset) clearInterval(int)

    }, 20)

}