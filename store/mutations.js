/**
 * scroll mutation
 * @param scroll
 */
export const scrolling = (state, scrolling) => {
    return state.scrolling = scrolling
}