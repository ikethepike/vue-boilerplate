///////////////////////////////////////////////////////////////////////////////////
// This is the main imports file, here you can find all the necessary components //
///////////////////////////////////////////////////////////////////////////////////

/*** File structure  ***/
// -- Templates 
// -- Pages
// -- Elements
// -- Icons 

// Templates
require("./templates")

// Pages
require("./pages")

// Elements
require("./elements")

// Icons 
require("./icons")