///////////////////////////////////////////////////////////////////////
// Elements are sitewide, reusable blocks like headers, footers etc. //
///////////////////////////////////////////////////////////////////////

// -- Siteheader
Vue.component("siteheader", require("../components/elements/siteheader/Siteheader.vue"))

// -- Sitefooter
Vue.component("sitefooter", require("../components/elements/sitefooter/Sitefooter.vue"))

// -- Text
// -- -- Ticker (Scrolling text)
Vue.component("ticker", require("../components/elements/text/Ticker.vue"))

// -- Progress
// -- Radial
Vue.component("radial", require("../components/elements/progress/Radial.vue"))