////////////////////////////////////
// Icon imports as Vue components //
////////////////////////////////////

//  Logo imports

// -- Normal
Vue.component("logo", require("../components/icons/logo/Logo.vue"))

// -- Large
Vue.component("logoLarge", require("../components/icons/logo/Logo-large.vue"))

// -- Small
Vue.component("logoSmall", require("../components/icons/logo/Logo-small.vue"))

// -- Text
Vue.component("logoText", require("../components/icons/logo/Logo-text.vue"))
