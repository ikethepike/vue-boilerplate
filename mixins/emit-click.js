// Emit event "clicked" on click
export default { 
    methods : {
        itemClick()
        {
            this.$emit("clicked")
        }
    }
}