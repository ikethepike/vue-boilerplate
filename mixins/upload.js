/* Generic File upload, using axios */
export default {
    props : {
        endpoint : {
            default : window.location.hostname + "/api/file/upload",
            type    : String
        },
        send  : {
            default : 'file',
            type    : String
        }
    },
    data() {
        return {
            errors      : [],
            // States
            uploading   : false
        }
    },
    methods : {
        upload(e)
        {
            this.uploading = true

            return axios.post(this.endpoint, this.packageUploads(e) ).then((response) => {
                this.uploading = false
                return Promise.resolve(response);
            })
            .catch((error) => {
                this.uploading = false

                return Promise.reject(error)
            })
        },
        packageUploads(e)
        {
            let fileData = new FormData()

            fileData.append(this.send, e.target.files[0])

            return fileData
        }
    }
}