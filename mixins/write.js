export default {
    methods : {
        data() {
            return {

                // Write out texts
                letterText          : "",
                wordText            : "",

                // Transition speed
                transpeed           : 1000,

                // Iterations
                writeIterations     : 0,

                // Containers
                processing          : {
                    letter  : "",
                    word    : ""
                },
                tempProcessing      : []
            }
        },
        writeByLetter(text, totalTime = 3000)
        {
            this.tempProcessing = text.split("")

            this.tempProcessing = this.tempProcessing.filter(function( element ) {
               return element !== undefined
            })

            this.calculateTransition(totalTime)

            this.stringalizer()

        },
        stringalizer()
        {

                setTimeout(function(){

                    if(this.letterText){
                        this.letterText = this.letterText + this.tempProcessing[this.writeIterations]
                    } else {
                        this.letterText = this.tempProcessing[this.writeIterations]
                    }

                // Separate words and characters

                if(this.writeIterations + 1 >= this.tempProcessing.length){
                    return true
                } else {
                    this.stringalizer()
                    this.writeIterations++
                }

            }.bind(this), this.transpeed)

        },
        calculateTransition(totalTime)
        {

            var total = this.writeIterations =  0

            total = this.tempProcessing.length

            this.transpeed = totalTime / total

        }
    }
}