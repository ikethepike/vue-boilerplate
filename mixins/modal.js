// Generic modal mixin 

export default {
    props : {
        modalOpen : {
            default : false,
        }
    },
    methods : {
        closeModal  : {
            this.$emit("closeModal")
        }
    }
}