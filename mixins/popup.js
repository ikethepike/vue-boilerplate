// Default methods and variables for popups
export default {
    props : {
        open : {
            default     : false
        },
        message : {
            type        : String,
            required    : true
        },
        button  : {
            default     : "Confirm"
        }
    },
    methods : {
        closePopup()
        {
            this.$emit("close")
        }
    }
}