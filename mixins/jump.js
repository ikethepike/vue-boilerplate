// Basic Jump methods 
export default {
    methods : {
        jump(distance = window.innerHeight)
        {
            // If distance is under 1, then assume decimal percent
            if(distance < 1){
                distance = window.innerHeight * distance
            }

            return this.scrollTo(distance)
        },
        jumpTo(el = null)
        {
            if(!el){
                return false;
            }

            return this.scrollto(offset(document.querySelector(el)).top)
        },
        scrollTo(amount)
        {
            return window.scrollTo(0, amount)
        },
        offset(el) 
        {
            var rect = el.getBoundingClientRect(),
            scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
        }
    }
}