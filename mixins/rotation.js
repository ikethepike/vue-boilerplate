/* Device rotation data */ 
export default {
    data() {
        return {
            rotation : {alpha : 0, beta : 0, gamma : 0 }
        }
    },
    mounted()
    {
        window.addEventListener("deviceorientation", function(e){
            this.rotation = {
                alpha   : e.alpha,
                beta    : e.beta,
                gamma   : e.gamma
            }
        }.bind(this))
    }
}