/*


 ______     __   __       __     ______     __  __        ______   __  __     ______        __   __   __  __     ______
/\  ___\   /\ "-.\ \     /\ \   /\  __ \   /\ \_\ \      /\__  _\ /\ \_\ \   /\  ___\      /\ \ / /  /\ \/\ \   /\  ___\
\ \  __\   \ \ \-.  \   _\_\ \  \ \ \/\ \  \ \____ \     \/_/\ \/ \ \  __ \  \ \  __\      \ \ \'/   \ \ \_\ \  \ \  __\
 \ \_____\  \ \_\\"\_\ /\_____\  \ \_____\  \/\_____\       \ \_\  \ \_\ \_\  \ \_____\     \ \__|    \ \_____\  \ \_____\
  \/_____/   \/_/ \/_/ \/_____/   \/_____/   \/_____/        \/_/   \/_/\/_/   \/_____/      \/_/      \/_____/   \/_____/

    Vue boilerplate and folder structure designed for Laravel projects
    By @ikethepike 2017
 */

// Initialize all dependencies
require("./init.js")

// Import all components
require("./imports/index")

// Vuex
import store from "./store"

const app = new Vue({
    el: '#app',
    store,
})

// Let's initialize any vanilla JS
// require("vanilla");
